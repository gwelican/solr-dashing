class Dashing.Smp extends Dashing.Widget

  ready: ->
    # This is fired when the widget is done being rendered

  onData: (data) ->
    node = $(@node)
    node.removeClass('status-green')
    node.removeClass('status-red')
    node.removeClass('status-orange')
    node.addClass "status-#{data.status}"
