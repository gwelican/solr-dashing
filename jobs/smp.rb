require 'net/http'
require 'open-uri'
require 'cgi'
require 'json'
require 'time'
require 'mysql2'


SCHEDULER.every '15s' do

  #db = Mysql2::Client.new(:host => "192.168.1.1", :username => "dashing", :password => "SECRET", :port => 3306, :database => "users" )
  #sql = "select issue.*, c.id,market from (select id,market from load_report order by id desc) as c left join issue on issue.load_report_id = c.id group by market"
  
  #results = db.query(sql)
  #
  statuses = ["green","orange","red"]

  send_event('smp_TCBE', {value: "TCBE", status: statuses.sample})
  send_event('smp_TCUK', {value: "TCUK", status: statuses.sample})
  send_event('smp_NECBE', {value: "NECBE", status: statuses.sample})
  send_event('smp_VUNL', {value: "VUNL", status: statuses.sample})
  send_event('smp_NECNL', {value: "NECNL", status: statuses.sample})

end

